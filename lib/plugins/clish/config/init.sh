#!/bin/sh

PLUGIN_VERSION="0.0.1"
PLUGIN_DESCRIPTION="Get/Set $CLI_MAIN_COMMAND config"
PLUGIN_AUTHORS="Greg"

__CONFIG_KEYS_LIST=""

list_config_from() {
  CONFIG_FILE=$1
  CONFIG_KEY=$2
  if [ -e "$CONFIG_FILE" ]; then
    while read LINE; do
      LINE=`echo $LINE | sed -e 's/#.*$//'`
      if [ ! "x$LINE" = "x" ]; then
        FOUND_CONFIG_KEY=`echo $LINE | sed -e 's/=.*$//'`
        FOUND_CONFIG_VALUE=`echo $LINE | sed -e 's/^.*=//'`
        if [ "x$CONFIG_KEY" = "x" ] || [ "$CONFIG_KEY" = "$FOUND_CONFIG_KEY" ] ; then
          CONFIG_KEY_PRESENT=`echo $__CONFIG_KEYS_LIST | grep ":$FOUND_CONFIG_KEY"`
          if [ "x$CONFIG_KEY_PRESENT" = "x" ] ; then
            __CONFIG_KEYS_LIST=$__CONFIG_KEYS_LIST:$FOUND_CONFIG_KEY
            echo "$FOUND_CONFIG_KEY : $(eval echo \$$FOUND_CONFIG_KEY)"
          fi
        fi
      fi
    done < "$CONFIG_FILE"
  fi
}

list_config() {
  __CONFIG_KEYS_LIST=""
  list_config_from "$CLI_USER_CONFIG"
  list_config_from "$CLI_DEFAULT_CONFIG"
}

get_config() {
  KEY=$1
  __CONFIG_KEYS_LIST=""
  list_config_from "$CLI_USER_CONFIG" "$KEY"
  list_config_from "$CLI_DEFAULT_CONFIG" "$KEY"
}

reset_config() {
  KEY=$1
  CONFIG_KEY_PRESENT=`cat "$CLI_DEFAULT_CONFIG" | grep "^$KEY"`
  if [ "x$CONFIG_KEY_PRESENT" = "x" ] ; then
    >&2 echo "<!!> Invalid config key $KEY"
    exit 1
  fi

  [ -f "$CLI_USER_CONFIG" ] && sed -i -e "/$KEY=.*/d" $CLI_USER_CONFIG
}

set_config() {
  KEY=$1
  VALUE=$2
  CONFIG_KEY_PRESENT=`cat "$CLI_DEFAULT_CONFIG" | grep "^$KEY"`
  if [ "x$CONFIG_KEY_PRESENT" = "x" ] ; then
    >&2 echo "<!!> Invalid config key $KEY"
    exit 1
  fi


  [ -d $(dirname "$CLI_USER_CONFIG") ] || mkdir -p $(dirname "$CLI_USER_CONFIG")
  [ -f "$CLI_USER_CONFIG" ] || touch "$CLI_USER_CONFIG"
  CONFIG_KEY_PRESENT=`cat "$CLI_USER_CONFIG" | grep "^$KEY"`
  if [ "x$CONFIG_KEY_PRESENT" = "x" ] ; then
    echo "$KEY=\"$VALUE\"" >> "$CLI_USER_CONFIG"
  else
    sed -i -e "s#$KEY=.*#$KEY=\"$VALUE\"#" "$CLI_USER_CONFIG"
  fi
}
